package com.my;

import org.openqa.selenium.WebDriver;

public class HomePage {
	
	private TopPart topPart;

	public HomePage(WebDriver driver) {
		topPart = new TopPart(driver);
	}

	public ShoppingCartEmptyPage clickOnShoppingCartMenu() {
		topPart.clickOnShoppingCartMenu();
		return topPart.newShoppingCartEmptyPage();
	}

}
