package com.my;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ShoppingCartEmptyPage extends TopPart {
	
	private WebElement shoppingCartLabel;

	private WebElement shoppingCartContentLabel;
	
	private WebElement continueButton;
	
	public ShoppingCartEmptyPage(WebDriver driver) {
		super(driver);
		shoppingCartLabel = driver.findElement(By.xpath("//div[@id='content']/h1"));
		shoppingCartContentLabel = driver.findElement(By.xpath("//div[@id='content']/p"));
		continueButton = driver.findElement(By.xpath("//a[text()='Continue']"));
	}
	
	public String getShoppingCartContentLabel() {
		return shoppingCartContentLabel.getText();
	}
	
	public String getShoppingCartLabel() {
		return shoppingCartLabel.getText();
	}
	
	public HomePage clickContinueButton() {
		continueButton.click();
		return new HomePage(driver);
	}

}
