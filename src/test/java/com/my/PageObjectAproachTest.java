package com.my;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class PageObjectAproachTest {

	private WebDriver driver;

	@BeforeMethod
	void setUp() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
	}

	@AfterMethod
	void tearDown() {
		driver.quit();
	}

	@Test
	void testIfShoppingChartIsEmptyAndContinueButtonOpensHomePage() {
		driver.get("https://demo.opencart.com");
		ShoppingCartEmptyPage shoppingCartEmptyPage = new HomePage(driver).clickOnShoppingCartMenu();
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals("Your shopping cart is empty!", shoppingCartEmptyPage.getShoppingCartContentLabel());
		shoppingCartEmptyPage.clickContinueButton();
		softAssert.assertEquals("Your Store", driver.getTitle());
		softAssert.assertAll();
	}

}