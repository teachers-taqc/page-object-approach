package com.my;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TopPart {
	
	protected WebDriver driver;
	
	private WebElement shoppingCartMenu;
	
	public TopPart(WebDriver driver) {
		this.driver = driver;
		shoppingCartMenu = driver.findElement(By.xpath("//a[@title='Shopping Cart']"));
	}

	public ShoppingCartEmptyPage clickOnShoppingCartMenu() {
		shoppingCartMenu.click();
		return new ShoppingCartEmptyPage(driver);
	}

	public ShoppingCartEmptyPage newShoppingCartEmptyPage() {
		return new ShoppingCartEmptyPage(driver);
	}
	
}
